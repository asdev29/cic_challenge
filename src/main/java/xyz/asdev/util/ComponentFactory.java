package xyz.asdev.util;

import xyz.asdev.persistence.MoviesCSV;
import xyz.asdev.persistence.MoviesDao;
import xyz.asdev.service.MoviesService;
import xyz.asdev.service.MoviesServiceImpl;

/**
 * The component factory provides methods to create the core components of the application.
 */
public class ComponentFactory {

    /**
     * Creates a new {@link MoviesDao} instance.
     * @return a new object of {@link MoviesCSV}
     */
    public static MoviesDao createMoviesDao(){
        return new MoviesCSV();
    }

    /**
     * Creates a new {@link MoviesService} instance.
     * @return a new object of {@link MoviesServiceImpl}
     */
    public static MoviesService createMoviesService(){
        return new MoviesServiceImpl();
    }
}

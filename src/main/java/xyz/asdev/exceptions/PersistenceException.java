package xyz.asdev.exceptions;

/**
 * Thrown by errors in persistence layer
 */
public class PersistenceException extends Exception {

    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}

package xyz.asdev.exceptions;
/**
 * Thrown by errors in service layer
 */
public class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}

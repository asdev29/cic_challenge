package xyz.asdev.api;

import xyz.asdev.exceptions.ServiceException;
import xyz.asdev.model.Movie;
import xyz.asdev.model.ErrorMessage;
import xyz.asdev.service.MoviesService;
import xyz.asdev.util.ComponentFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * Class for handling HTTP requests for movies
 */
@Path("movies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MoviesApi {


    private MoviesService moviesService;

    public MoviesApi() {
        this.moviesService = ComponentFactory.createMoviesService();
    }

    /**
     * Method handling HTTP GET request. The returned object will be sent
     * to the client as "json" media type.
     *
     * @return response as json ,where response entity is list of movies
     * @param title used for filtering movies by title.If null,all movies are retrieved.
     */
    @GET
    public Response getMovies(@QueryParam("title") final String title) {
        try {
            List<Movie> res = moviesService.getMovies(title);
            return Response.status(Response.Status.OK).entity(res).build();
        } catch (ServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorMessage(e.getMessage())).build();
        }

    }
}

package xyz.asdev.model;


import com.opencsv.bean.CsvBindByPosition;

public class Movie {

    @CsvBindByPosition(position=0)
    private String title;
    @CsvBindByPosition(position=2)
    private String location;

    public Movie() {
    }

    public Movie(String title, String location) {
        this.title = title;
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}

package xyz.asdev.service;

import xyz.asdev.exceptions.PersistenceException;
import xyz.asdev.exceptions.ServiceException;
import xyz.asdev.model.Movie;
import xyz.asdev.persistence.MoviesDao;
import xyz.asdev.util.ComponentFactory;

import java.util.ArrayList;
import java.util.List;

public class MoviesServiceImpl implements MoviesService {

    private MoviesDao moviesDao;

    public MoviesServiceImpl() {
        this.moviesDao = ComponentFactory.createMoviesDao();
    }

    @Override
    public List<Movie> getMovies(String title) throws ServiceException {
        try {
            List<Movie> res = moviesDao.getAllMovies();
            if(title==null){
                return res;
            }else{
                List<Movie> temp = new ArrayList<>();
                for(Movie m : res){
                    if(m.getTitle().toLowerCase().contains(title.toLowerCase())){
                        temp.add(m);
                    }
                }
                return temp;
            }
        } catch (PersistenceException e) {
            throw new ServiceException("Error by loading movies!",e);
        }
    }
}

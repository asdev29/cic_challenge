package xyz.asdev.service;


import xyz.asdev.exceptions.ServiceException;
import xyz.asdev.model.Movie;

import java.util.List;

/**
 * Defines contract for business logic of application.
 */
public interface MoviesService {
    /**
     * Retrieves all movies from persistence layer and filter them by title.
     * @param title used for filtering movies
     * @return list of movies which title contains passed parameter. If passed parameter is null
     * all movies are retrieved.
     * @throws ServiceException thrown if some exception occurs during the retrievement of movies
     */
    List<Movie> getMovies(String title) throws ServiceException;
}

package xyz.asdev.persistence;


import xyz.asdev.exceptions.PersistenceException;
import xyz.asdev.model.Movie;

import java.util.List;

/**
 * Defines a contract for communication between business logic layer(service) and data source.
 */
public interface MoviesDao {

     /**
      * Retrieves data from data source and parses it into list of Movies.
      * @return list of all movies
      * @throws PersistenceException thrown if some exception occurs during the retrievement of data
      */
     List<Movie> getAllMovies() throws PersistenceException;
}

package xyz.asdev.persistence;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import xyz.asdev.exceptions.PersistenceException;
import xyz.asdev.model.Movie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.List;

public class MoviesCSV implements MoviesDao {

    private static final String FILE_NAME = "/movies.csv";

    public List<Movie> getAllMovies() throws PersistenceException {
        URL url = getClass().getResource(FILE_NAME);
        if(url==null){
            throw new PersistenceException("There is no such resource with this name!");
        }
        File file = new File(url.getFile());
        CsvToBean<Movie> csvToBean = null;
        try {
            csvToBean = new CsvToBeanBuilder<Movie>(new FileReader(file))
                    .withSeparator(';')
                    .withType(Movie.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSkipLines(1)
                    .build();
            return  csvToBean.parse();
        } catch (FileNotFoundException e) {
            throw new PersistenceException("File with movies is not found!",e);
        }
    }

}

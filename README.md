# CIC_Challenge

Rest Service for retrieving titles and filming locations of movies which have been filmed in San Francisco. Data is represented in JSON format.



## Usage
#### Launching application
In order to start Rest Service use following maven commands:
```
mvn clean compile exec:java
```
After launching, you can visit :
###### For retrieving all movies 
[http://localhost:8082/cic_challenge/movies](http://localhost:8082/cic_challenge/movies) 
###### For retrieving movies whose title contains '180'
[http://localhost:8082/cic_challenge/movies?title=180](http://localhost:8082/cic_challenge/movies?title=180)
##### *Instead of '180' you can use any text for filtering movies.

#### Stopping application
You can stop Service at any time by pressing **Enter** in command prompt.





#
